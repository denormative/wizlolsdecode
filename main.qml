import QtQuick 2.2
import QtQuick.Controls 1.1
import WizLoLS 1.0

ApplicationWindow {
	id: app
	visible: true
	width: 1024
	height: 768
	title: qsTr("Hello World")

	menuBar: MenuBar {
		Menu {
			title: qsTr("File")
			MenuItem {
				text: qsTr("Exit")
				onTriggered: Qt.quit();
			}
		}
	}

	property string mapPrefix: "/Users/darke/Wizardry/dataEnglish/map/"

	ListModel {
		id: wizMapModel
		ListElement { mapSuffix: "map001"; mapName: "Dungeon of Trials B1F"; }
		ListElement { mapSuffix: "map002"; mapName: "Dungeon of Trials B2F"; }
		ListElement { mapSuffix: "map003"; mapName: "Dungeon of Trials B3F"; }
		ListElement { mapSuffix: "map004"; mapName: "Dungeon of Trials B4F"; }
		ListElement { mapSuffix: "map005"; mapName: "Dungeon of Trials B5F"; }
		ListElement { mapSuffix: "map006"; mapName: "Dungeon of Trials B6F"; }
		ListElement { mapSuffix: "map007"; mapName: "Dungeon of Trials B7F"; }
		ListElement { mapSuffix: "map008"; mapName: "Dungeon of Trials B8F"; }
		ListElement { mapSuffix: "map009"; mapName: "Dungeon of Trials B9F"; }
		ListElement { mapSuffix: "map010"; mapName: "Dungeon of Trials B10F"; }
		ListElement { mapSuffix: "map011"; mapName: " B1F"; }
		ListElement { mapSuffix: "map012"; mapName: " B2F"; }
		ListElement { mapSuffix: "map013"; mapName: " B3F"; }
		ListElement { mapSuffix: "map014"; mapName: " B4F"; }
		ListElement { mapSuffix: "map015"; mapName: " B5F"; }
		ListElement { mapSuffix: "map016"; mapName: " B6F"; }
		ListElement { mapSuffix: "map017"; mapName: " B7F"; }
		ListElement { mapSuffix: "map018"; mapName: " B8F"; }
		ListElement { mapSuffix: "map019"; mapName: " B9F"; }
		ListElement { mapSuffix: "map020"; mapName: " B10F"; }
	}

	property string infoData: ""
	property string triggerData: "(Click on Map)"

	ListView {
//		anchors.fill: parent
		anchors.top: parent.top
		anchors.bottom: parent.bottom
		anchors.left: parent.left
		width: 32*22
		anchors.margins: 8
		//anchors.top: parent.top
		id: wizMapView
		focus: true
		pixelAligned: true
		spacing: 16

		delegate: Item {
			id: map
			width: childrenRect.width
			height: childrenRect.height

			Column {
				Row {
					Text {
						text: mapName
					}
					Text {
						text: " (" + mapSuffix + ")"
						font.capitalization: Font.AllUppercase
					}
				}
				WizMap {
					mapName: app.mapPrefix + mapSuffix + ".dat"
					MouseArea {
						anchors.fill: parent
						onClicked: { console.log("button clicked"); app.infoData = parent.getTileInfo((mouseX/32)-1, (mouseY/32)-1); app.triggerData = parent.getTriggerData(); }
						onPositionChanged: app.infoData = parent.getTileInfo((mouseX/32)-1, (mouseY/32)-1);
						hoverEnabled: true
					}
				}

			}

		}
		model: wizMapModel
	}

	Text {
		id: infoData
		anchors.top: parent.top
		anchors.left: wizMapView.right
		anchors.right: parent.right
		anchors.margins: 8

		text: app.infoData
		font.family: "Courier"
	}
	Flickable {
		anchors.top: infoData.bottom
		anchors.bottom: parent.bottom
		anchors.left: wizMapView.right
		anchors.right: parent.right
		anchors.margins: 8
		 contentWidth: childrenRect.width; contentHeight: childrenRect.height
		 focus: true
		 Column {
			 Text {
				 text: "Trigger Values"
				 font.family: "Courier"
			 }
			 Text {
				 text: app.triggerData
				 font.family: "Courier"
			 }

		 }

	}

}
