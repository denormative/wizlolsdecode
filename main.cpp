#include <QApplication>
#include <QQmlApplicationEngine>

#include "WizMap.h"

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	qmlRegisterType<WizMap>("WizLoLS", 1, 0, "WizMap");

	QQmlApplicationEngine engine;

	engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

	return app.exec();
}
