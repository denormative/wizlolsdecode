#ifndef WIZMAP_H
#define WIZMAP_H

#include <QString>
#include <QQuickItem>
#include <QQuickPaintedItem>
#include <QBrush>
#include <QPainter>
#include <QDebug>
#include <QHash>
#include <cassert>
#include <QClipboard>
#include <QApplication>

const int mapTileLen = 20;

class File {
public:
	void open() {
		QFile file(name);
		if (!file.open(QIODevice::ReadOnly))
			qt_assert("false", __FILE__, __LINE__);//return;

		QDataStream in(&file);
		data.resize(file.size());
		in.readRawData(data.data(), data.length());
	}

	QString name;
	QByteArray data;
};

class MapTile {
public
	:
	enum {
		Is010203 = 0,
		IsDark = 1,
		IsNull02 = 2,
		WallsAndDoors = 3,
		WallTile_LowByte = 4,
		WallTile_HighByte = 5,
		DungeonProp_LowByte = 6,
		DungeonProp_HighByte = 7,
		TileEffect_Id = 8,
		TileEffect_b01 = 9,
		TileEffect_b02 = 10,
		TileEffect_b03 = 11,
		TileEffect_param = 12,
		Event_Direction = 13,
		Event_LowByte = 14,
		Event_HighByte = 15,

	};

	enum {
		North = 0x01,
		East  = 0x04,
		South = 0x10,
		West  = 0x40,
	};

	QByteArray tile;

	MapTile(QByteArray newTile=QByteArray()) : tile(newTile) {
		if(tile.size()) {
			// just some sanity checks.
			assert(tile.at(Is010203)==0x01 || tile.at(Is010203)==0x02 || tile.at(Is010203)==0x03);
			assert((tile.at(IsDark)>>4)==0); // only bottom 4 bits are used
			assert(tile.at(IsNull02)==0); // always zero?
		}
	}

	void debug() {
		qDebug() << (int)tile.at(0);
	}

	bool isSolid() const {
		// hack for 'empty' MapTiles; we'll always pretend we don't need to do anything
		if(tile.size()==0)
			return true;
		else
			return tile.at(WallsAndDoors)==0x55;
	}

	bool isDark() {
		//assert(tile.at(IsDark)==0x00 || tile.at(IsDark)&0x08 || tile.at(IsDark)&0x04 || tile.at(IsDark)&0x02 || tile.at(IsDark)&0x01);
		return tile.at(IsDark)==0x08;
	}

	bool isDarkBit01() {
		qDebug() << (int)tile.at(IsDark);
		return tile.at(IsDark)==0x01;
	}

	bool isDarkBit02() {
		qDebug() << (int)tile.at(IsDark);
		return tile.at(IsDark)==0x02;
	}

	bool isDarkBit04() {
		qDebug() << (int)tile.at(IsDark);
		return tile.at(IsDark)==0x04;
	}

	int wallTiles() {
		return at(WallTile_LowByte) + (at(WallTile_HighByte)<<8);
	}

	int wallTileNorth() { return (wallTiles()&0x000F)>>0;  }
	int wallTileEast()  { return (wallTiles()&0x00F0)>>4;  }
	int wallTileSouth() { return (wallTiles()&0x0F00)>>8;  }
	int wallTileWest()  { return (wallTiles()&0xF000)>>12; }

	int dungeonPropNorthWest() { return (tile.at(DungeonProp_LowByte)&0x0F)>>0;  }
	int dungeonPropNorthEast()  { return (tile.at(DungeonProp_LowByte)&0xF0)>>4;  }
	int dungeonPropSouthEast() { return (tile.at(DungeonProp_HighByte)&0x0F)>>0;  }
	int dungeonPropSouthWest()  { return (tile.at(DungeonProp_HighByte)&0xF0)>>4; }

	int eventDirection() {
		return at(Event_Direction);
	}

	int eventCode() {
		return at(Event_LowByte) + (at(Event_HighByte)<<8);
	}

	unsigned int at(int i) const {
		return (unsigned int)(unsigned char)tile.at(i);
	}
};

class MapTrigger {
public:
	MapTrigger(QByteArray newTrigger=QByteArray()) : trigger(newTrigger) {

	}

	QByteArray trigger;
};

class MapFile : public File {
public:
	MapTile at(const int x, const int y) {
		const int hashId = y*20+x;
		if(tiles.contains(hashId))
			return tiles.value(hashId);
		else {
			MapTile tile(data.mid(mapTileLen*(hashId), mapTileLen));
			tiles.insert(hashId, tile);
			return tile;
		}
	}
	MapTrigger trigger(const int i) {
		const int hashId = i;
		if(triggers.contains(hashId))
			return triggers.value(hashId);
		else {
			MapTrigger trigger(data.mid(8000+14*(hashId), 14));
			triggers.insert(hashId, trigger);
			return trigger;
		}
	}
	const int maxTriggers = 138;

	QHash<int, MapTile> tiles;
	QHash<int, MapTrigger> triggers;
};

class WizMap : public QQuickPaintedItem
{
	Q_OBJECT
	Q_PROPERTY(QString mapName READ getMapName WRITE setMapName NOTIFY mapNameChanged)

	public:
		WizMap(QQuickItem *parent = 0);
		void paint(QPainter *painter);

		Q_INVOKABLE QString getTileInfo(int x, int y);
		Q_INVOKABLE QString getTriggerData();

		QString getMapName();
		void setMapName(QString mapName);

		MapFile mapFile;
	private:
		QString mapName;

		const int sz = 32;
		const int dd = sz/16; //door depth
		const int dw = sz/4; //door width
	signals:
		void mapNameChanged();
};

inline WizMap::WizMap(QQuickItem *parent)
	: QQuickPaintedItem(parent)
	, mapName("")
{
	setWidth((3+sz)*20);
	setHeight((3+sz)*20);

}

inline void resetColor(QPainter *painter) {
	painter->setPen(Qt::black);
	painter->setBrush(Qt::black);
}

inline void setColor(QPainter *painter, const int i) {
	switch(i) {
		case 0x02:
			painter->setPen(Qt::magenta);
			painter->setBrush(Qt::magenta);
			break;
		case 0x05:
			painter->setPen(Qt::cyan);
			painter->setBrush(Qt::cyan);
			break;
		case 10:
			painter->setPen(Qt::yellow);
			painter->setBrush(QBrush(Qt::yellow, Qt::DiagCrossPattern));
			break;
		default:
			resetColor(painter);
			break;
	}
}

inline QString liftBitfield2String(const int i) {
	return QString("%1%2%3%4%5")
			.arg(i&0x10 ? '1' : '0')
			.arg(i&0x08 ? '1' : '0')
			.arg(i&0x04 ? '1' : '0')
			.arg(i&0x02 ? '1' : '0')
			.arg(i&0x01 ? '1' : '0');
}

inline void WizMap::paint(QPainter *painter)
{
	qDebug() << this->mapName;

	QBrush brush(Qt::black);//QColor(Qt::white));
	painter->setBrush(brush);
	painter->setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
	painter->setRenderHint(QPainter::Antialiasing, false);


	QFont font("New", 18);
	//font.setStyleStrategy(QFont::NoAntialias);
	painter->setFont(font);

	painter->fillRect(0, 0, (2+sz)*20*20, (2+sz)*20*20, Qt::white);

	for(int y = 0; y < 20; y++) {
		QRect rect(0*sz, (20-y)*sz, sz, sz);
		painter->drawText(rect, Qt::AlignCenter, QString("%1").arg(y, 2, 10, QLatin1Char('0')));
	}

	for(int x = 0; x < 20; x++) {
		QRect rect((1+x)*sz, (1+20)*sz, sz, sz);
		painter->drawText(rect, Qt::AlignCenter, QString("%1").arg(x, 2, 10, QLatin1Char('0')));
	}

	QFont font_small("New", 9);
	painter->setFont(font_small);

	for(int y = 0; y < 20; y++) {
		for(int x = 0; x < 20; x++) {
			MapTile tile = mapFile.at(x, y);

			QPoint topLeft((1+x)*sz, (1+y)*sz);
			QPoint topRight((1+x+1)*sz-1, (1+y)*sz);
			QPoint bottomLeft((1+x)*sz, (1+y+1)*sz-1);
			QPoint bottomRight((1+x+1)*sz-1, (1+y+1)*sz-1);

			resetColor(painter);

			// if the four sides indicate all walls are blocked, just crosshatch
			if(tile.isSolid()) {
				painter->fillRect(QRect(topLeft, QSize(sz, sz)), Qt::DiagCrossPattern);
				continue;
			}

			if(tile.isDark())
				painter->fillRect(QRect(topLeft, QSize(sz, sz)), Qt::Dense6Pattern);

			resetColor(painter);

			QRect rect(topLeft, bottomRight);
			unsigned int param = tile.at(MapTile::TileEffect_param);

			switch(tile.at(MapTile::TileEffect_Id)) {
				case 0x00:
					// nothing
					break;
				case 0x05:
					painter->drawText(rect, Qt::AlignCenter, QString("EXIT\nTOWN"));
					assert(tile.at(MapTile::TileEffect_b01)==0 && tile.at(MapTile::TileEffect_b02)==0 && tile.at(MapTile::TileEffect_b03)==0);
					assert(tile.at(MapTile::TileEffect_param)==0x65);
					break;
				case 0x06:
					painter->drawText(rect, Qt::AlignCenter, QString("EXIT\n%1").arg(param+1, 0, 10, QLatin1Char(' ')));
					assert(tile.at(MapTile::TileEffect_b01)==0 && tile.at(MapTile::TileEffect_b02)==0 && tile.at(MapTile::TileEffect_b03)==0);
					break;
				case 0x0C:
					painter->drawText(rect, Qt::AlignCenter, QString("LIFT\n%1").arg(liftBitfield2String(param)));
					assert(tile.at(MapTile::TileEffect_b01)==0 && tile.at(MapTile::TileEffect_b02)==0 && tile.at(MapTile::TileEffect_b03)==0);
					break;
				case 0x0D:
					painter->drawText(rect, Qt::AlignCenter, QString("MOVE\n%1").arg(param, 0, 10, QLatin1Char(' ')));
					assert(tile.at(MapTile::TileEffect_b01)==0 && tile.at(MapTile::TileEffect_b02)==0 && tile.at(MapTile::TileEffect_b03)==0);
					break;
				case 0x0E:
					painter->drawText(rect, Qt::AlignCenter, QString("SPIN"));
					assert(tile.at(MapTile::TileEffect_b01)==0 && tile.at(MapTile::TileEffect_b02)==0 && tile.at(MapTile::TileEffect_b03)==0);
					assert(tile.at(MapTile::TileEffect_param)==0x00);
					break;
				case 0x0F:
					painter->drawText(rect, Qt::AlignCenter, QString("WARP\n%1").arg(param, 0, 10, QLatin1Char(' ')));
					assert(tile.at(MapTile::TileEffect_b01)==0 && tile.at(MapTile::TileEffect_b02)==0 && tile.at(MapTile::TileEffect_b03)==0);
					break;
				case 0x10:
					painter->fillRect(QRect(topLeft, QSize(sz, sz)), QBrush(Qt::yellow, Qt::SolidPattern));
					break;
				case 0x11:
					painter->drawText(rect, Qt::AlignCenter, QString("SKULL"));
					assert(tile.at(MapTile::TileEffect_b01)==0 && tile.at(MapTile::TileEffect_b02)==0 && tile.at(MapTile::TileEffect_b03)==0);
					assert(tile.at(MapTile::TileEffect_param)==0x00);
					break;
				case 0x18:
					painter->drawText(rect, Qt::AlignCenter, QString("WOOD\nCHEST\n%1").arg(param, 0, 10, QLatin1Char(' ')));
					assert(tile.at(MapTile::TileEffect_b01)==0 && tile.at(MapTile::TileEffect_b02)==0 && tile.at(MapTile::TileEffect_b03)==0);
					break;
				case 0x1A:
					painter->drawText(rect, Qt::AlignCenter, QString("SWTCH\n%1").arg(param, 0, 10, QLatin1Char(' ')));
					assert(tile.at(MapTile::TileEffect_b01)==0 && tile.at(MapTile::TileEffect_b02)==0 && tile.at(MapTile::TileEffect_b03)==0);
					break;
				default:
					painter->drawText(rect, Qt::AlignCenter, QString("??\n??"));
					QString t = QString("%1,%2: %3 %4 %5 %6 %7").arg(x, 2, 10, QLatin1Char('0')).arg(y, 2, 10, QLatin1Char('0'))
							.arg(tile.at(MapTile::TileEffect_Id), 2, 16, QLatin1Char('0'))
							.arg(tile.at(MapTile::TileEffect_b01), 2, 16, QLatin1Char('0'))
							.arg(tile.at(MapTile::TileEffect_b02), 2, 16, QLatin1Char('0'))
							.arg(tile.at(MapTile::TileEffect_b03), 2, 16, QLatin1Char('0'))
							.arg(tile.at(MapTile::TileEffect_param), 2, 16, QLatin1Char('0'));
					qDebug() << t;
					break;
			}

			MapTile south = (y==19) ? MapTile() : mapFile.at(x, y+1);
			MapTile north = (y==0) ? MapTile() : mapFile.at(x, y-1);
			MapTile east = (x==19) ? MapTile() : mapFile.at(x+1, y);
			MapTile west = (x==0) ? MapTile() : mapFile.at(x-1, y);

			painter->drawPoint(topLeft);
			painter->drawPoint(topRight);
			painter->drawPoint(bottomLeft);
			painter->drawPoint(bottomRight);

			QSize vertDoor(dd, dw*2);
			QSize horizDoor(dw*2, dd);

			// top wall & door
			if((tile.tile.at(3)&MapTile::North) || (tile.tile.at(3)&0x02)) {
				resetColor(painter);
				if(north.isSolid())
					painter->drawLine(topLeft-QPoint(0, 1), topRight-QPoint(0, 1));
				painter->drawLine(topLeft, topRight);
			}
			setColor(painter, tile.wallTileNorth());
			if((tile.tile.at(3)&0x02))
				painter->drawRect(QRect(topLeft+QPoint(dw, 0), horizDoor));

			// right wall & door
			if((tile.tile.at(3)&MapTile::East) || (tile.tile.at(3)&0x08)) {
				resetColor(painter);
				if(east.isSolid())
					painter->drawLine(topRight+QPoint(1, 0), bottomRight+QPoint(1, 0));
				painter->drawLine(topRight, bottomRight);
			}
			setColor(painter, tile.wallTileEast());
			if((tile.tile.at(3)&0x08))
				painter->drawRect(QRect(topRight+QPoint(-dd, dw), vertDoor));

			// bottom wall & door
			if((tile.tile.at(3)&MapTile::South) || (tile.tile.at(3)&0x20)) {
				resetColor(painter);
				if(south.isSolid())
					painter->drawLine(bottomLeft+QPoint(0, 1), bottomRight+QPoint(0, 1));
				painter->drawLine(bottomLeft, bottomRight);
			}
			setColor(painter, tile.wallTileSouth());
			if((tile.tile.at(3)&0x20))
				painter->drawRect(QRect(bottomLeft+QPoint(dw, -dd), horizDoor));

			// left wall & door
			if((tile.tile.at(3)&MapTile::West) || (tile.tile.at(3)&0x80)) {
				resetColor(painter);
				if(west.isSolid())
					painter->drawLine(topLeft-QPoint(1, 0), bottomLeft-QPoint(1, 0));
				painter->drawLine(topLeft, bottomLeft);
			}
			setColor(painter, tile.wallTileWest());
			if((tile.tile.at(3)&0x80))
				painter->drawRect(QRect(topLeft+QPoint(0, dw), vertDoor));

		}
	}
}

inline QString bitfield2String(const int i) {
	return QString("%1%2%3%4%5%6%7%8")
			.arg(i&0x80 ? '1' : '0')
			.arg(i&0x40 ? '1' : '0')
			.arg(i&0x20 ? '1' : '0')
			.arg(i&0x10 ? '1' : '0')
			.arg(i&0x08 ? '1' : '0')
			.arg(i&0x04 ? '1' : '0')
			.arg(i&0x02 ? '1' : '0')
			.arg(i&0x01 ? '1' : '0');
}

inline QString WizMap::getTileInfo(const int x, const int y) {
	if(!(x>=0 && x<20) || !(y>=0 && y<20))
		return "";

	MapTile tile = mapFile.at(x, y);

	QString raw("%1,%2: %3 %4 %5 %6 %7 %8 %9 %10 %11 %12 %13 %14 %15 %16 %17 %18 %19 %20 %21 %22");

	raw = raw.arg(x, 2, 10, QLatin1Char('0')).arg(y, 2, 10, QLatin1Char('0'));

	for(int i=0; i<20; i++)
		raw = raw.arg((int)(unsigned char)(tile.tile.at(i)), 2, 16, QLatin1Char('0'));

	QClipboard *p_Clipboard = QApplication::clipboard();
	p_Clipboard->setText(raw);

	QString ret;

	ret += QString("(%1, %2)\n").arg(x, 2, 10, QLatin1Char('0')).arg(y, 2, 10, QLatin1Char('0'));
	ret += QString("123 byte: %1\n").arg((int)(unsigned char)tile.tile.at(MapTile::Is010203));
	ret += QString("Area Effect: %1\n").arg(bitfield2String(tile.tile.at(MapTile::IsDark)));
	ret += QString("Walls and Doors: %1\n").arg(bitfield2String(tile.tile.at(MapTile::WallsAndDoors)));

	ret += QString("Wall Tiles: N:%1 E:%2 S:%3 W:%4\n")
			.arg(tile.wallTileNorth(), -2)
			.arg(tile.wallTileEast(),  -2)
			.arg(tile.wallTileSouth(), -2)
			.arg(tile.wallTileWest(),  -2);

	ret += QString("Dungeon Props: NW:%1 NE:%2 SE:%3 SW:%4\n")
			.arg(tile.dungeonPropNorthWest(), -2)
			.arg(tile.dungeonPropNorthEast(),  -2)
			.arg(tile.dungeonPropSouthEast(), -2)
			.arg(tile.dungeonPropSouthWest(),  -2);

	ret += QString("Tile Effect Id: %1\n").arg(tile.at(MapTile::TileEffect_Id), 2, 16, QLatin1Char('0'));
	ret += QString("Tile Effect Unknown: %1 %2 %3\n")
			.arg(tile.at(MapTile::TileEffect_b01), 2, 16, QLatin1Char('0'))
			.arg(tile.at(MapTile::TileEffect_b02), 2, 16, QLatin1Char('0'))
			.arg(tile.at(MapTile::TileEffect_b03), 2, 16, QLatin1Char('0'));
	ret += QString("Tile Effect Param: %1\n").arg(tile.at(MapTile::TileEffect_param), 2, 16, QLatin1Char('0'));
	ret += QString("Event Direction: %1\n").arg(bitfield2String(tile.eventDirection()));
	ret += QString("Event Code: %1\n").arg(tile.eventCode());
	{
		QString unknown("%1 %2 %3 %4\n");
		for(int i=16; i<16+4; i++)
			unknown = unknown.arg((int)(unsigned char)(tile.tile.at(i)), 2, 16, QLatin1Char('0'));
		ret += unknown;
	}

	return raw + "\n" + ret;
}

inline QString WizMap::getTriggerData() {
	QString ret;

	for(int i=0; i<mapFile.maxTriggers-1; i++) {
		MapTrigger trigger = mapFile.trigger(i);
		QString raw("%1: %2 %3 %4 %5 %6 %7 %8 %9 %10 %11 %12 %13 %14 %15\n");
		raw = raw.arg(i, 3, 10);
		for(int i=0; i<14; i++)
			raw = raw.arg((int)(unsigned char)(trigger.trigger.at(i)), 2, 16, QLatin1Char('0'));
		ret += raw;
	}
//	MapTrigger trigger = mapFile.trigger(mapFile.maxTriggers-1);
//	QString raw("%1: %2 %3 %4 %5 %6 %7");
//	raw = raw.arg(mapFile.maxTriggers-1, 3, 10);
//	for(int i=0; i<6; i++)
//		raw = raw.arg((int)(unsigned char)(trigger.trigger.at(mapFile.maxTriggers-1)), 2, 16, QLatin1Char('0'));


	return ret;
}

inline QString WizMap::getMapName()
{
	return this->mapName;
}

inline void WizMap::setMapName(QString mapName)
{
	this->mapName = mapName;
	this->mapFile.name = mapName;
	this->mapFile.open();
}

#endif // WIZMAP_H
